const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});

app.use("/", express.static("../front-end"));

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});

connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), email  VARCHAR(255) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10), cnp VARCHAR(20), varsta VARCHAR(3), gen VARCHAR(1))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});

app.post("/bilet", (req, res) => {

  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
    gen: ""
  };
  let error = [];
  let zi_inceput = 0;
  let luna_inceput = 0;
  let an_inceput = 0;
  let zi_sfarsit = 0;
  let luna_sfarsit = 0;
  let an_sfarsit = 0;
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ
  if (!bilet.nume||!bilet.prenume||!bilet.telefon||!bilet.email||!bilet.cnp||!bilet.data_inceput||!bilet.data_sfarsit||!bilet.varsta) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
    if (bilet.nume.length < 3 || bilet.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!bilet.nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (bilet.prenume.length < 3 || bilet.prenume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!bilet.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (bilet.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!bilet.email.match("^[A-Za-z]+@[A-Za-z]+\.[A-Za-z]+$")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    if (!bilet.data_inceput.match("^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$")) {
      console.log("Data inceput invalida!");
      error.push("Data inceput invalida!");
      zi_inceput = (bilet.data_inceput.charAt(0) - '0') * 10 + bilet.data_inceput.charAt(1) - '0';
      luna_inceput = (bilet.data_inceput.charAt(3) - '0') * 10 + bilet.data_inceput.charAt(4) - '0'; 
      an_inceput = (bilet.data_inceput.charAt(6) - '0') * 1000 + (bilet.data_inceput.charAt(7) - '0') * 100 + (bilet.data_inceput.charAt(8) - '0') * 10 + (bilet.data_inceput.charAt(9) - '0');
    }
    if (!bilet.data_sfarsit.match("^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$")) {
      console.log("Data sfarsit invalida!");
      error.push("Data sfarsit invalida!");
      zi_sfarsit = (bilet.data_sfarsit.charAt(0) - '0') * 10 + bilet.data_sfarsit.charAt(1) - '0';
      luna_sfarsit = (bilet.data_sfarsit.charAt(3) - '0') * 10 + bilet.data_sfarsit.charAt(4) - '0'; 
      an_sfarsit = (bilet.data_sfarsit.charAt(6) - '0') * 1000 + (bilet.data_sfarsit.charAt(7) - '0') * 100 + (bilet.data_sfarsit.charAt(8) - '0') * 10 + (bilet.data_sfarsit.charAt(9) - '0');
    }
    if(zi_inceput + 40 * (luna_inceput - 1) + 480 * an_inceput > zi_sfarsit + 40 * (luna_sfarsit - 1) + 480 * an_sfarsit) {
      console.log("Data sfarsit inainte de data inceput!");
      error.push("Data sfarsit inainte de data inceput!");
    }
    if(bilet.cnp.length != 13 || !bilet.cnp.match("^[0-9]{13}$")|| !bilet.cnp.charAt(0).match("^[1-6]$")) {
      console.log("CNP invalid!");
      error.push("CNP invalid!");
    } else {
      if(bilet.cnp.charAt(0) == '1' || bilet.cnp.charAt(0) == '3' || bilet.cnp.charAt(0) == '5') {
        bilet.gen = "M";
      }
      if(bilet.cnp.charAt(0) == '2' || bilet.cnp.charAt(0) == '4' || bilet.cnp.charAt(0) == '6') {
        bilet.gen = "F";
      }
    }
    if(bilet.varsta.length > 3 || bilet.varsta.length < 1 || !bilet.varsta.match("^[0-9]+$")) {
      console.log("Varsta invalida!");
      error.push("Varsta invalida!");
    }
    const unique = 'SELECT * FROM abonamente_metrou WHERE email = ' + mysql.escape(bilet.email);
    connection.query(
      unique,
      function (err, result) {
        console.log(result);
        if (err) throw err;
        if(result.length > 0) {
          if(result) {
            console.log("Email existent!");
            error.push("Email existent!");
          } 
        }
        if (error.length === 0) {
  

          const sql = `INSERT INTO abonamente_metrou (nume,
            prenume,
            telefon,
            cnp,
            email,
            data_inceput,
            data_sfarsit,
            varsta,
            gen) VALUES (?,?,?,?,?,?,?,?,?)`;
          connection.query(
            sql,
            [
              bilet.nume,
              bilet.prenume,
              bilet.telefon,
              bilet.cnp,
              bilet.email,
              bilet.data_inceput,
              bilet.data_sfarsit,
              bilet.varsta,
              bilet.gen,
            ],
            function (err, result) {
              if (err) throw err;
              console.log("Abonament realizat cu succes!");
              res.status(200).send({
                message: "Abonament realizat cu succes",
              });
              console.log(sql);
            }
          );
        } else {
          res.status(500).send(error);
          console.log("Abonamentul nu a putut fi creat!");
        }
      }
    )
  }

 

  
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html